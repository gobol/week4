       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. THANATHAT.
       

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  CITY-CODE PIC 9 VALUE ZERO .
           88 CITY-IS-DUBLIN VALUE 1.
           88 CITY-IS-LIMEMRICK VALUE 2.
           88 CITY-IS-CORK VALUE 3.
           88 CITY-IS-GALWAY VALUE 4.
           88 CITY-IS-SLIGO VALUE 5.
           88 CITY-IS-WATERFOAD VALUE 6.
           88 UNIVERSITY-CITY VALUE 1 THRU 4.
           88 CITY-CODE-NOT-VALID VALUE 0,7,8,9.
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a City Code (1-6) - " WITH NO ADVANCING 
           ACCEPT  CITY-CODE 

           IF CITY-CODE-NOT-VALID THEN
              DISPLAY "Invalid city code entered"
           ELSE
              IF CITY-IS-LIMEMRICK  THEN
                 DISPLAY  "Hey, We're Home."
              END-IF 
              IF CITY-IS-DUBLIN THEN
                 DISPLAY  "Hey, We're in the Capital."
              END-IF
              IF UNIVERSITY-CITY THEN
                 DISPLAY "Apply the rent Surcharge!"
              END-IF
           END-IF
           SET CITY-IS-DUBLIN TO TRUE 
           DISPLAY CITY-CODE
           GOBACK 
           .
           
